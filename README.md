# TP4_Eventos_y_callbacks


Usando como base el código dado en la consigna, escribir un programa que permita seleccionar
una porción rectangular de una imagen, luego:

- Con la letra “g” guardar la porción de la imagen seleccionada como una nueva
imagen

- Con la letra “r” restaurar la imagen original y permitir realizar una nueva selec-
ción,

- Con la “q” finalizar.